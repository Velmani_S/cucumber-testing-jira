# README #

**Description of the framework:**

In this framework I used Cucumber BDD framework + Selenium WebDriver + Page object pattern. Mainly it’s a kind of Hybrid frameworks: 

1. It’s keyword driven (we have gherkin keywords and code which implements each keyword). 

2. It’s data driven (it’s possible to use parameters inside the keywords). In order to recognize parameters inside the keywords - I’ve specified them in double quotes (like And I choose "Bug" issuetype, where Bug is parameter). 

I created 3 feature files for 3 tests: 

* Each feature file(like BugCreation.feature) has tag(like @create) which is used when maven run the tests. In this way we can skip particular tests or add them to test run. List of tags to be used in test run is configured in jira-app-project/pom.xml file (line 63)

* At the bottom of the feature files you can see the table with parameters. Each row from such table means 1 test run of this feature with parameters specified on this line. In order to skip some test runs from the table - RunMode column was introduced. When it’s Y - we run the test, When it’s N - we skip it.

* Framework supports crossbrowser testing in Chrome, FireFox, IE browsers(this time I haven’t tried to run tests in IE cause there is no version of IE browser for OS X).


**How it works:**

1) Cucumber tests are running with the help of junit

2) According to the settings in pom.xml - appropriate scenarios will be taken(currently they are selected by tag). Scenarios are located in jira-app-project/src/main/resorces/ (e.g. BugCreation.feature)

3) The cucumber finds implementations for the steps(like Given RunMode is "<RunMode>") in jira-app-project/src/main/java/ recursively (in this case CommonUtil.java and BugCreation.java) 

4) Steps implementations use methods of the object of WebConnector class (Selenium) to perform the actions against the elements. And then I use assertions to check if the result of each action is correct.

5) WebConnector operates with elements provided by the pages containing them (page object pattern), so in case of GUI changing - there will be only 1 place to update the locator for the affected element.



**How to use framework and run the tests:**

Pre-conditions:
java and maven have to be installed on your environment

Steps:

1) Import project to your IDE

2) Change target location for the output log file(it’s creating during test execution, useful for debugging) in log4j.properties located in jira-app-project/src/main/resources

3) In order to use Chrome - open jira-app-project/src/main/java/com/jira/config/config.properties, find line 6 and change the value of chromeEnv parameter according to your environment(MAC, Linux, WIN.exe). Currently it's set to MAC 

4) Open terminal and cd to jira-app-project directory

5) Fire the following commands:
 - mvn eclipse:eclipse
 - mvn compile
 - mvn integration-test

6) Check how the tests are running

7) Observe results in jira-app-project/target/cucumber-html-report/index.html

8) Observe file Application.log in the directory you've set in 2nd step if needed