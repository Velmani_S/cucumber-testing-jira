package com.jira.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
//import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
//import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.jira.pages.DashboardPage;
import com.jira.pages.NewIssuePage;
import com.jira.pages.SignInPage;
import com.jira.pages.SignOutPage;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
//import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;




public class WebConnector {
	
	public String FAIL = "FAIL";
	public String PASS = "PASS";
	public String RandomValue = "RandomValue";
	public String screenshotsFolder=null;
	
	
	Properties CONFIG = null;
	public WebDriver driver = null;
	WebDriver mozilla = null;
	WebDriver chrome = null;
	WebDriver ie = null;
	WebDriverWait wait = null;
	Logger APP_LOGS=Logger.getLogger("devpinoyLogger");
	
	//declaring the pages with elements
	SignInPage loginPage;
	DashboardPage dashboardPage;
	SignOutPage signOutPage;
	NewIssuePage newIssuePage;
	
	public static WebConnector w;
	
	private WebConnector(){
		
		//initializing config file
		if (CONFIG == null){
			try{					
				FileInputStream fs = new FileInputStream(System.getProperty("user.dir")+"/src/main/java/com/jira/config/config.properties");
				CONFIG = new Properties();								
				CONFIG.load(fs);
			}catch(Exception e){
				log("Error in properties initialization - " + e.getMessage());
			}
		}
	}
	
		// List of Application independent methods
		public String openBrowser(String browserType){
			log("Opening browser");
			try{
				//opening Mozilla browser
				if(browserType.equals("Mozilla") && mozilla == null){ // if browser isn't opened - this method will open browser
					driver=new FirefoxDriver();
					mozilla = driver;
				}
				else if(browserType.equals("Mozilla") && mozilla != null){ // if browser is already opened - this method will reuse opened browser
					driver = mozilla;
				}
			
				//opening IE browser
				else if(browserType.equals("IE") && ie == null){
					driver=new InternetExplorerDriver();
					ie = driver;
				}
				else if(browserType.equals("IE") && ie != null){
					driver = ie;
				}
				
				//opening Chrome browser
				else if(browserType.equals("Chrome") && chrome == null){
					String environment=CONFIG.getProperty("chromeEnv");
					System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/src/main/java/com/jira/config/chromedriver"+environment);
					driver=new ChromeDriver();
					chrome = driver;}
				else if(browserType.equals("Chrome") && chrome != null){
					driver = chrome;}
			
				//maximize browser window
				driver.manage().window().maximize();
			
				//implicit wait implementation
				long implicitWaitTime=Long.parseLong(CONFIG.getProperty("implicitwait"));
				driver.manage().timeouts().implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
				log("Browser " + browserType + " was opened successfully");
			return PASS; 
			}catch(Exception e){
				log("Browser failed to open " + e.getMessage() + "  " + e.getStackTrace());
				e.printStackTrace();
				return FAIL;
			}		
		}
		
		public String closeBroswer(){
			log("Closing the browser");			
			try{
				driver.close();
				log("Browser was closed");
				return PASS;
				}catch(Exception e){
				log("Unable to close the browser "+ e.getMessage());
				return FAIL;
				}
		}
		
		public WebDriver getDriver(){
			return driver;
		}
		
		public String navigate(String URL){			
			log("Navigating to URL: " + URL);
			try{
				driver.navigate().to(CONFIG.getProperty(URL));
				log("Successfully redirected to " + URL);
				return PASS;
				}catch(Exception e){			
					log("Cannot navigate to the the url: " + URL + " " + e.getMessage());	
					return FAIL;
				}			
		}
		
		public String pause(String data) throws NumberFormatException, InterruptedException{
			log("Making a pause for " + data + " seconds");
			try{
				long time = (long)Double.parseDouble(data);
				Thread.sleep(time*1000L);
				log("Pause for - " +data + " seconds was made");
				return PASS;
				}catch(Exception e){
					log("Pause wasn't made " + e.getMessage());
					return FAIL;
				}
		}
		
		public  String synchronize(){
			try{
				((JavascriptExecutor) driver).executeScript(
	        		"function pageloadingtime()"+
	        				"{"+
	        				"return 'Page has completely loaded'"+
	        				"}"+
	        		"return (window.onload=pageloadingtime());");
				return PASS;
				}catch(Exception e){
					return FAIL;
				}
		}
		
		public String typeWebElement(String text, WebElement element){			
			log("typing the text " + text + " into the " + element);
			try{
				element.sendKeys(text);
				log("Successfully typed the text " + text + " into the " + element);
				return PASS;
			}catch(Exception e){
				log("Cannot type the text into input field. Probably field wasn't found " + e.getMessage());	
				return FAIL;	
			}
		}
		
		public String clickWebElement(WebElement element){		    
			log("clicking on the " + element);
			try{
				element.click();
				log("Successfully clicked on " + element);
				return PASS;
			}catch(Exception e){
				log("Cannot click on element. Probably element wasn't found " + e.getMessage());	
				return FAIL;
					
			}
			
		}
			
		public String doDefaultLogin (String browser){
			log("Doing default login");
			try{
				openBrowser(browser);	
				navigate("loginURL");
				loginPage = SignInPage.getInstance(driver);
				typeWebElement(CONFIG.getProperty("defaultLoginName"), loginPage.getLoginField());					
				typeWebElement(CONFIG.getProperty("defaultPassword"), loginPage.getPasswordField());
				clickWebElement(loginPage.getSignInButton());
				synchronize();
				log("default login was done successfully");
				return PASS;
			}catch(Exception e){
				log("Default login wasn't made " + e.getMessage());	
			return FAIL;
			}
		}
		
		public String logOut(){
			log("Signing out");			
			try{
				dashboardPage = DashboardPage.getInstance(driver);
				clickWebElement(dashboardPage.getHeaderDetailsMenu());
				clickWebElement(dashboardPage.getLogOut());
				signOutPage = SignOutPage.getInstance(driver);
				clickWebElement(signOutPage.getLogOutOption());
				log("User successfully signed out");
				return PASS;
				}catch(Exception e){
				log("Unable to sign out "+ e.getMessage());
				return FAIL;
				}
		}
		
		public  String selectList(WebElement dropList, String itemToSelect){		
			log("Selecting " +itemToSelect+ " from list " + dropList);
			try{
				dropList.clear();
				dropList.sendKeys(itemToSelect);
				dropList.sendKeys(Keys.ENTER);
				log(itemToSelect + " was selected");
	
			}catch(Exception e){
					log("Could not select from list. "+ e.getMessage());
					return FAIL;	
			}
			log("Selection item in the drop-list was successfull");
			return PASS;			
		}
		
		public  String hitEnter(WebElement element){
			
			log("Hitting the Enter");
			try{
				element.sendKeys(Keys.ENTER);
	
			}catch(Exception e){
					log("Could not hit Enter. "+ e.getMessage());
					return FAIL;	
			}
			log("Successfully hit Enter");
			return PASS;			
		}
		
		public String refreshPage (){
			log("refreshing the page");
			try{
				driver.navigate().refresh();
			}catch(Exception e){
				log("page wasn't refreshed");
				return FAIL;
			}
			return PASS;
		}
		
		public String getElementText(WebElement object){
			log("getting the text of the element" + object);	
			String actualText="";
			try{
				actualText = object.getText();
			}catch(Exception e){
				log("unable to get text of the element, "+ e.getMessage());
				return FAIL;	
			}
			return 	actualText;
		}
		
		public String clearElement(WebElement object){
			log("Clearing the element" + object);	
			try{
			object.clear();
			}catch(Exception e){
				log("element was not cleared, "+ e.getMessage());
				return FAIL;	
			}
			log("element was cleared");
			return 	PASS;
		}
		
		public String checkTextInsideElement(WebElement object, String expectedText){
			log("Verifying the presence of the text in the element" + object);	
			String actualText = object.getText();
				if(actualText.contains(expectedText)){
					log("Element " + object + " contains the expected text" + expectedText);
					return PASS;
				}else{
					log("Element " + object + " does not contain the expected text" + expectedText);
					return FAIL;
				}
		}
		
	
		//Using singletone to get the instance of WebConnector
		public static synchronized WebConnector getInstance (){
			if (w==null){
				w = new WebConnector();
			}
			return w;
		}
		//methods which logs custom messages from the other methods
		
		public void log(String msg){
			APP_LOGS.debug(msg);	
		}
	
	

}
