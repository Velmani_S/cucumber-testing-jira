package com.jira.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/*defining the list of private elements on the page IssuesFilterPage. 
 *the getters return WebElement 
 */
public class IssuesFilterPage {
	
	private static IssuesFilterPage issuesFilterPage;	
	private final WebDriver driver;
	
	private IssuesFilterPage(WebDriver dr){
		driver = dr;
	}
	
	private WebElement firstFoundIssueLink;
	private WebElement searcherQueryFileld;
	
	public WebElement getFirstFoundIssueLink(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		firstFoundIssueLink = wait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText("HOM-")));
		return firstFoundIssueLink;
	}
	
	public WebElement getSearcherQueryFileld(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		searcherQueryFileld = wait.until(ExpectedConditions.elementToBeClickable(By.id("searcher-query")));
		return searcherQueryFileld;
	}
	
	//Using singletone to get the instance of NewIssuePage
		public static synchronized IssuesFilterPage getInstance (WebDriver dr){
			if (issuesFilterPage==null){
				issuesFilterPage = new IssuesFilterPage(dr);
			}
			return issuesFilterPage;
		}
	
	

}




	
	
	
	
	
