package com.jira.pages;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/*defining the list of private elements on the page ExistingIssuePage. 
 *the getters return WebElement 
 */

public class ExistingIssuePage {
	
	private static ExistingIssuePage existingIssuePage;	
	private final WebDriver driver;
	
	private ExistingIssuePage(WebDriver dr){
		driver = dr;
	}
	
	private WebElement summaryField;
	private WebElement descriptionField;
	private WebElement descriptionFieldWithPencil;
	private WebElement editIssueButton;
	private WebElement issueUpdateButton;
	
	
	
	
	public WebElement getDescriptionFieldWithPencil(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		descriptionFieldWithPencil = wait.until(ExpectedConditions.elementToBeClickable(By.id("description-val")));
		return descriptionFieldWithPencil;
	}
	
	public WebElement getDescriptionField(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		descriptionField = wait.until(ExpectedConditions.elementToBeClickable(By.id("description")));
		return descriptionField;
	}
	
	public WebElement getSummaryField(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		summaryField = wait.until(ExpectedConditions.elementToBeClickable(By.id("summary-val")));
		return summaryField;
	}
	
	public WebElement getEditIssueButton(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		editIssueButton = wait.until(ExpectedConditions.elementToBeClickable(By.id("edit-issue")));
		return editIssueButton;
	}
	
	public WebElement getIssueUpdateButton(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		issueUpdateButton = wait.until(ExpectedConditions.elementToBeClickable(By.id("edit-issue-submit")));
		return issueUpdateButton;
	}	
	
	
	//Using singletone to get the instance of NewIssuePage
		public static synchronized ExistingIssuePage getInstance (WebDriver dr){
			if (existingIssuePage==null){
				existingIssuePage = new ExistingIssuePage(dr);
			}
			return existingIssuePage;
		}

}
