package com.jira.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/*defining the list of private elements on the page SignIn. 
 *the getters return WebElement 
 */

public class SignInPage {
	
	private static SignInPage signInPage;
	private final WebDriver driver;
	
	private SignInPage(WebDriver dr){	
		driver = dr;	
	}

	private WebElement loginField;
	private WebElement passwordField;
	private WebElement signInButton;	
	
	public WebElement getLoginField(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		loginField = wait.until(ExpectedConditions.elementToBeClickable(By.id("username")));
		return loginField;
	}
	public WebElement getPasswordField(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		loginField = wait.until(ExpectedConditions.elementToBeClickable(By.id("password")));
		return loginField;
	}
	public WebElement getSignInButton(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		signInButton = wait.until(ExpectedConditions.elementToBeClickable(By.id("login")));
		return signInButton;
	}
	

	//Using singletone to get the instance of SignInPage
			public static synchronized SignInPage getInstance (WebDriver dr){
				if (signInPage==null){
					signInPage = new SignInPage(dr);					
				}
				return signInPage;
			}

}
