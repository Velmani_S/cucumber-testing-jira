package com.jira.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/*defining the list of private elements on the page SignOut. 
 *the getters return WebElement 
 */

public class SignOutPage {

	private static SignOutPage signOutPage;	
	private final WebDriver driver;
	
	private SignOutPage(WebDriver dr){
		driver = dr;
	}
	
	private WebElement logOutOption;
	
	
	public WebElement getLogOutOption(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		logOutOption = wait.until(ExpectedConditions.elementToBeClickable(By.id("logout")));
		return logOutOption;
	}
	
	//Using singletone to get the instance of NewIssuePage
		public static synchronized SignOutPage getInstance (WebDriver dr){
			if (signOutPage==null){
				signOutPage = new SignOutPage(dr);
			}
			return signOutPage;
		}	
}
