package com.jira.testcases.issueManaging;

import com.jira.pages.DashboardPage;
import com.jira.pages.ExistingIssuePage;
import com.jira.pages.IssuesFilterPage;
import com.jira.pages.NewIssuePage;
import com.jira.pages.SignInPage;
import com.jira.pages.SignOutPage;

import org.junit.Assert;

import com.jira.util.WebConnector;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

/*BugCreation class contains methods which implement keywords of the appropriate feature*/
public class BugCreation {
	SignInPage loginPage;
	DashboardPage dashboardPage;
	SignOutPage signOutPage;
	NewIssuePage newIssuePage;
	ExistingIssuePage existingIssuePage;
	
	WebConnector Selenium = WebConnector.getInstance();
	
	
	//When I start the creation of new issue
	@When ("^I start the creation of new issue$") 
		public void startNewFeatureCreation ()  {
		dashboardPage = DashboardPage.getInstance(Selenium.getDriver());
		String result = Selenium.clickWebElement(dashboardPage.getCreateNewIssueButton());	
		Assert.assertEquals(result, Selenium.PASS);
		}
	
	//And I choose "Bug" issuetype
	@And ("^I choose \"([^\"]*)\" issuetype$") 
	public void selectIssueType (String issueType)  {
		newIssuePage = NewIssuePage.getInstance(Selenium.getDriver());
		String result = Selenium.selectList(newIssuePage.getIssueTypeDropDown(), issueType);		
		Assert.assertEquals(result, Selenium.PASS);		
	}
	
	//And I specify the Summary as "<bugSummary>"
	@And ("^I specify the Summary as \"([^\"]*)\"$") 
	public void specifyIssueSummary (String issueSummary)  {
		newIssuePage = NewIssuePage.getInstance(Selenium.getDriver());
		String result = Selenium.typeWebElement(issueSummary, newIssuePage.getSummaryField());		
		Assert.assertEquals(result, Selenium.PASS);
		}
	//And I choose "<bugPriority>" priority
	@And ("^I choose \"([^\"]*)\" priority$") 
	public void specifyIssuePriority (String issuePriority)  {		
		newIssuePage = NewIssuePage.getInstance(Selenium.getDriver());
		String result = Selenium.selectList(newIssuePage.getPriorityDropDown(), issuePriority);
		Assert.assertEquals(result, Selenium.PASS);
		}
	
	//And I choose the "Component" which contains the issue
	@And ("^I choose the \"([^\"]*)\" which contains the issue$") 
	public void chooseIssueComponent (String issueComponent)  {		
		newIssuePage = NewIssuePage.getInstance(Selenium.getDriver());
		String result = Selenium.selectList(newIssuePage.getComponentsDropDown(), issueComponent);
		Assert.assertEquals(result, Selenium.PASS);
		}
	
	//And I specify "1.0.0" Fix version 
	@And ("^I specify \"([^\"]*)\" Fix version$") 
	public void chooseFixVersion (String issueFixVersion)  {	
		newIssuePage = NewIssuePage.getInstance(Selenium.getDriver());
		String result = Selenium.selectList(newIssuePage.getFixVersionsDropDown(), issueFixVersion);
		Assert.assertEquals(result, Selenium.PASS);
		}
	
	//And I assign issue on me
	@And ("^I assign issue on me$") 
	public void assignIssueOnMe ()  {		
		newIssuePage = NewIssuePage.getInstance(Selenium.getDriver());
		String result = Selenium.clickWebElement(newIssuePage.getAssignOnMe());
		Assert.assertEquals(result, Selenium.PASS);
		}
	
	//And I specify the issue description as "<bugDescription>"
	@And ("^I specify the issue description as \"([^\"]*)\"$") 
	public void fillIssueDescription (String descriptionText)  {
		newIssuePage = NewIssuePage.getInstance(Selenium.getDriver());
		String result = Selenium.typeWebElement(descriptionText, newIssuePage.getDescriptionField());
		Assert.assertEquals(result, Selenium.PASS);
		}
	
	//And I create the issue
	@And ("^I create the issue$") 
	public void finishIssueCreation ()  {		
		newIssuePage = NewIssuePage.getInstance(Selenium.getDriver());
		String result = Selenium.clickWebElement(newIssuePage.getCreateIssueButton());
		Assert.assertEquals(result, Selenium.PASS);
		}
	
	//Then I should see the successful message "<message>"
	@And ("^I should see the successful message \"([^\"]*)\"$") 
	public void CheckIssueCreationMessage (String message)  {	
		dashboardPage = DashboardPage.getInstance(Selenium.getDriver());
		String result = Selenium.checkTextInsideElement(dashboardPage.getNotificationWindow(), message);
		Assert.assertEquals(result, Selenium.PASS);	
		}
	
	//And Issue should be created with Summary
	@And ("^Issue should be created with Summary \"([^\"]*)\"$") 
	public void CheckIssueCreated (String summary)  {	
		dashboardPage = DashboardPage.getInstance(Selenium.getDriver());
		String result = dashboardPage.clickIssueLinkFromNotificationWindow();
		Assert.assertEquals(result, Selenium.PASS);
		existingIssuePage = ExistingIssuePage.getInstance(Selenium.getDriver());
		String actualSummaryText=Selenium.getElementText(existingIssuePage.getSummaryField());
		Assert.assertEquals(actualSummaryText, summary);
		}
}
