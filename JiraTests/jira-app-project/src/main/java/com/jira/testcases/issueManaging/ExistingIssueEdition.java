package com.jira.testcases.issueManaging;

import org.junit.Assert;

import com.jira.pages.DashboardPage;
import com.jira.pages.ExistingIssuePage;
import com.jira.pages.IssuesFilterPage;
import com.jira.pages.NewIssuePage;
import com.jira.pages.SignInPage;
import com.jira.pages.SignOutPage;

import com.jira.util.WebConnector;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/*ExistingIssueEdition class contains methods which implement keywords of the appropriate feature*/

public class ExistingIssueEdition {
	
	SignInPage loginPage;
	DashboardPage dashboardPage;
	SignOutPage signOutPage;
	NewIssuePage newIssuePage;
	ExistingIssuePage existingIssuePage;
	IssuesFilterPage issuesFilterPage;
	private String oldValue="";
	private String addedText="";
	
	WebConnector Selenium = WebConnector.getInstance();
	
	//And I open one of the issues
	@And ("^I open one of the issues$") 
	public void openIssue ()  {
		dashboardPage = DashboardPage.getInstance(Selenium.getDriver());
		String result = Selenium.clickWebElement(dashboardPage.getIssuesMenu());	
		Assert.assertEquals(result, Selenium.PASS);
	
		result = Selenium.clickWebElement(dashboardPage.getIssuesReportedByMe());	
		Assert.assertEquals(result, Selenium.PASS);

		issuesFilterPage = IssuesFilterPage.getInstance(Selenium.getDriver());
		result = Selenium.clickWebElement(issuesFilterPage.getFirstFoundIssueLink());	
		Assert.assertEquals(result, Selenium.PASS);
	}
	
	public void setOldDescription(String value){
		oldValue = value;
	}
	
	public void setAddedText(String value){
		addedText = value;
	}
	
	//And I add "<newBugDescription>" to the issue description 
	@And ("^I add \"([^\"]*)\" to the issue description$")
	public void updateIssueDescription (String descriptionToAdd)  {
		existingIssuePage = ExistingIssuePage.getInstance(Selenium.getDriver());	
		String result = Selenium.clickWebElement(existingIssuePage.getEditIssueButton());	
		Assert.assertEquals(result, Selenium.PASS);
		//getting current value from description field
		String currentValue=Selenium.getElementText(existingIssuePage.getDescriptionField());
		setOldDescription(currentValue);
		setAddedText(descriptionToAdd);
		//modifying description
		result = Selenium.clearElement(existingIssuePage.getDescriptionField());	
		Assert.assertEquals(result, Selenium.PASS);
		//typing to the element
		String text = currentValue + ". " + descriptionToAdd;
		result = Selenium.typeWebElement(text, existingIssuePage.getDescriptionField());	
		Assert.assertEquals(result, Selenium.PASS);			
	}
		
	//When I update the issue
	@And ("^I update the issue$")
	public void updateIssue ()  {
		existingIssuePage = ExistingIssuePage.getInstance(Selenium.getDriver());
		String result = Selenium.clickWebElement(existingIssuePage.getIssueUpdateButton());	
		Assert.assertEquals(result, Selenium.PASS);
		Selenium.log("synchronized");
	}
	
	//Then My changes should be applied
	@Then ("^My changes should be applied$")
	public void checkChangesApplied ()  {
		existingIssuePage = ExistingIssuePage.getInstance(Selenium.getDriver());
		String result = Selenium.refreshPage();	
		Assert.assertEquals(result, Selenium.PASS);
		
		String expectedModifiedValue=oldValue + ". " + addedText;
		String actualModifiedText=Selenium.getElementText(existingIssuePage.getDescriptionFieldWithPencil());
		Assert.assertEquals(actualModifiedText, expectedModifiedValue);
		
	}

}
